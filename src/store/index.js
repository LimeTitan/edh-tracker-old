import { createStore } from "vuex";

export default createStore({
  state: {
    settings: {

    },
    players: {
      one: {
        color: '#00328f',
        lifeTotal: 40,
        commanderDamage: {
          one: 0,
          two: 0,
          three: 0,
          four: 0
        },
        commanderTax: [0, 0],
        partner: false,
        commander: 2,
      },
      two: {
        color: 'rgb(138, 90, 0)',
        lifeTotal: 40,
        commanderDamage: {
          one: 0,
          two: 0,
          three: 0,
          four: 0
        },
        commanderTax: [0, 0],
        partner: false,
        commander: 2,
      },
      three: {
        color: 'rgb(148, 0, 0)',
        lifeTotal: 40,
        commanderDamage: {
          one: 0,
          two: 0,
          three: 0,
          four: 0
        },
        commanderTax: [0, 0],
        partner: true,
        commander: 2
      },
      four: {
        color: 'rgb(0, 128, 6)',
        lifeTotal: 40,
        commanderDamage: {
          one: [],
          two: [],
          three: [],
          four: 0
        },
        commanderTax: [0, 0],
        partner: false,
        commander: 1,
      }
    }
  },
  mutations: {
    incLifeTotal(state, player) {
      state.players[player].lifeTotal++
    },
    decLifeTotal(state, player) {
      state.players[player].lifeTotal--
    },
    initCommanderDamage(state, { player, opponent, commanderCount }) {
      for (let i = 0; i < commanderCount; i++) {
        state.players[player].commanderDamage[opponent][i] = 0
      }
    },
    incCommanderDamage(state, { player, opponent, partner }) {
      state.players[player].commanderDamage[opponent][partner]++
    },
    setCommanderDamage(state, { player, opponent, partner, newValue }) {
      // change Life Total
      state.players[player].lifeTotal += state.players[player].commanderDamage[opponent][partner] - newValue

      // change Commander Damage
      state.players[player].commanderDamage[opponent][partner] = newValue
    }
  },
  actions: {
    incLifeTotal({ commit }, player) {
      commit('incLifeTotal', player)
    },
    decLifeTotal({ commit }, player) {
      commit('decLifeTotal', player)
    },
    initCommanderDamage({ commit }, { player, opponent, commanderCount }) {
      commit('initCommanderDamage', { player, opponent, commanderCount })
    },
    incCommanderDamage({ commit }, { player, opponent, partner }) {
      commit('incCommanderDamage', { player, opponent, partner })
      commit('decLifeTotal', player)
    },
    setCommanderDamage({ commit }, { player, opponent, partner, newValue }) {
      commit('setCommanderDamage', { player, opponent, partner, newValue })
    }
  },
  modules: {},
});
