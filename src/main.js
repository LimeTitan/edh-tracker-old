import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import LongPress from "./directive/LongPress";

const app = createApp(App);

app.directive('longpress', LongPress);

app.use(store).use(router).mount("#app");
