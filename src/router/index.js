import { createRouter, createWebHashHistory } from "vue-router";
// import Home from "../views/Home.vue";
import FourPlayers from "../views/FourPlayers.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: FourPlayers,
  },
  {
    path: "/4",
    name: "FourPlayers",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/FourPlayers.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
